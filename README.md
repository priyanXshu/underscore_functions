
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

Complete the following underscore functions. <br>
Reference http://underscorejs.org/ for examples. <br>
Check and use MDN as well


    Create a function for each problem in a file called
        - keys.js
        - values.js
        - pairs.js
    and so on in the root of the project.
    Ensure that the functions in each file is exported and tested in its own file called
        - testKeys.js
        - testValues.js
        - testPairs.js
    and so on in a folder called test.
Create a new git repo on gitlab for this project, ensure that you commit after you complete each problem in the project.
    Ensure that the repo is a public repo.

When you are done, send the gitlab url to your mentor


function keys(obj) {
    <br>
    // Retrieve all the names of the object's properties.
    <br>
    // Return the keys as strings in an array.
    <br>
    // Based on http://underscorejs.org/#keys<br>
}

function values(obj) {<br>
    // Return all of the values of the object's own properties.<br>
    // Ignore functions<br>
    // http://underscorejs.org/#values<br>
}

function mapObject(obj, cb) {<br>
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.<br>
    // http://underscorejs.org/#mapObject<br>
}

function pairs(obj) {<br>
    // Convert an object into a list of [key, value] pairs.<br>
    // http://underscorejs.org/#pairs<br>
}

/* STRETCH PROBLEMS */

function invert(obj) {<br>
    // Returns a copy of the object where the keys have become the values and the values the keys.<br>
    // Assume that all of the object's values will be unique and string serializable.<br>
    // http://underscorejs.org/#invert<br>
}

function defaults(obj, defaultProps) {<br>
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.<br>
    // Return `obj`.<br>
    // http://underscorejs.org/#defaults<br>
}
