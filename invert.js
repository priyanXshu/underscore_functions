const invert = (obj) => {
  const invertedResult = {};
  for (const key in obj) {
    invertedResult[obj[key]] = key;
  }
  return invertedResult;
};

module.exports = invert;
